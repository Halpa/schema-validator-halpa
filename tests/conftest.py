import os
import sys

import pytest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Global setup for all tests (runs once)
def pytest_configure(config):
    config.addinivalue_line('markers', 'validator')
    config.addinivalue_line('markers', 'decorator')
