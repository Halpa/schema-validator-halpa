import json
import os
from pprint import pprint

import pytest

from validator import is_valid, validate
from validator.decorators import validate as validate_decorator
from validator.exception import ValidationException

test_schema = {
    "type": "object",
    "required": ["name", "age", "hired_at"],
    "definitions": {
        "marrigeTypes": {
            "enum": [
                "married",
                "single",
            ]
        },
    },
    "properties": {
        "name": {"type": "string"},
        "age": {"type": "number"},
        "hired_at": {
            "type": "string",
            "format": "date"
        },
        "pattern": {
            "type": "string",
            "format": "regex"
        },
        "readonly": {
            "type": "boolean",
        },
        "married": {
            "anyOf": [{ "$ref": "#definitions/marrigeTypes"}]
        }
    }
}

invalid_schema = {
    "type": "object",
    "required": ["name"],
    "properties": {
        "name": {"type": "sting"},
    }
}

@pytest.mark.validator
def test_validator_valid():
    
    exception_is_thrown = False

    try:
        validation = validate(schema=test_schema, data={
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
            "married": "married"
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown==False


@pytest.mark.decorator
def test_validator_decorator_valid():
    @validate_decorator(schema=test_schema)
    def decorator_test(data):
        return "expected_result"

    exception_is_thrown = False
    try:
        result = decorator_test({"body": {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
            "married": "married"
        }})
        assert result == "expected_result"
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown==False


@pytest.mark.decorator
def test_validator_decorator_valid_with_json_payload():
    @validate_decorator(schema=test_schema)
    def decorator_test(data):
        return "expected_result"

    exception_is_thrown = False
    try:
        result = decorator_test({"body": json.dumps({
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
            "married": "married"
        })})
        assert result == "expected_result"
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown==False


@pytest.mark.decorator
def test_validator_decorator_with_data_parameter_valid():

    @validate_decorator(schema=test_schema, where="param")
    def decorator_test(data):
        return "expected_result"

    exception_is_thrown = False
    try:
        result = decorator_test({"param": {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
            "married": "married"
        }})
        assert result == "expected_result"
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown==False


@pytest.mark.decorator
def test_validator_decorator_on_decorator_valid():

    def simpleDecorator(inner_f):
        def func(*args, **kwargs):
            return inner_f(*args, **kwargs)
        return func

    @simpleDecorator
    @validate_decorator(schema=test_schema, required=["married"])
    def decorator_test(data):
        return "expected_result"

    exception_is_thrown = False
    try:
        result = decorator_test({"body": {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
            "married": "married"
        }})
        assert result == "expected_result"
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown==False


@pytest.mark.decorator
def test_validator_decorator_invalid_input():

    @validate_decorator(schema=test_schema)
    def decorator_test(data):
        return "unexpected_result"
    
    exception_is_thrown = False
    try:
        decorator_test({"body": {
            "name": "Daniel",
            "hired_at": "1970-01-01",
            "married": "married"
        }})
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.errors == {'fields': {'age': 'age is required'}}

    assert exception_is_thrown


@pytest.mark.decorator
def test_validator_decorator_required():

    @validate_decorator(required=["name"])
    def decorator_test(data):
        return "unexpected_result"
    
    exception_is_thrown = False
    try:
        decorator_test({"body": {"name": "Daniel"}})
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown == False


@pytest.mark.decorator
def test_validator_decorator_required_missing():

    @validate_decorator(required=["name", "age"])
    def decorator_test(data):
        return "unexpected_result"
    
    exception_is_thrown = False
    try:
        decorator_test({"body": {"name": "Daniel"}})
    except ValidationException as e:
        exception_is_thrown = True
        assert e.errors == {'age': 'age is required'}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_invalid_if_not_dict():
    # Observe! Below uses json.dumps to cast data to string, which should not validate
    data = json.dumps({
        "name": "Daniel",
        "age": 30,
        "hired_at": "1970-01-01",
        "married": "married"
    })
    exception_is_thrown = False
    try:
        validate(schema=test_schema, data=data)
    except ValidationException as e:
        print(e.message)
        exception_is_thrown = True
        assert e.message=='Data is not a dictionary'

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_missing_required_1():
    
    exception_is_thrown = False
    try:
        validate(schema=test_schema, data={
            "age": 30,
            "hired_at": "1970-01-01",
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.message=='Data could not be validated'
        assert e.errors=={'fields': {'name': 'name is required'}}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_wrong_type():

    exception_is_thrown = False
    try:
        validation = validate(schema=test_schema, data={
            "name": "Daniel",
            "age": "thirty",
            "hired_at": "1970-01-01",
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.message=='Data could not be validated'
        assert e.errors=={'fields': {'age': "'thirty' is not of type 'number'"}}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_missing_required_and_wrong_type():

    exception_is_thrown = False
    try:
        validation = validate(schema=test_schema, data={
            "age": "thirty",
            "hired_at": "1970-01-01",
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.message=='Data could not be validated'
        assert e.errors=={'fields': {
            'name': 'name is required',
            'age': "'thirty' is not of type 'number'"
        }}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_wrong_format():

    exception_is_thrown = False
    try:
        validation = validate(schema=test_schema, data={
            "name": "Daniel",
            "age": 30,
            "hired_at": "last year",
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.message=='Data could not be validated'    
        assert e.errors=={'fields': {
            'hired_at': "'last year' is not a 'date'"
        }}

    assert exception_is_thrown    


@pytest.mark.validator
def test_validator_wrong_anyof_from_reference():

    exception_is_thrown = False
    try:        
        validation = validate(schema=test_schema, data={
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
            "married": "poly"
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.message=='Data could not be validated'
        assert e.errors=={'fields': {
            'married': "'poly' is not valid under any of the given schemas"
        }}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_with_file():

    exception_is_thrown = False
    try:    
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        schema_file = os.path.join(absolute_path, "schemas/test-schema.json")
        validation = validate(schema_file, {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01"
        })

    except ValidationException as e:
        print(e.message)
        exception_is_thrown = True

    assert exception_is_thrown == False


@pytest.mark.validator
def test_validator_with_file_with_relative_path():

    exception_is_thrown = False
    try:
        os.environ["PY_SCHEMA_PATH"] = os.path.dirname(os.path.abspath(__file__)) + "/test-relative-path"
        schema_file = "test-schema-in-relative.json"
        validation = validate(schema_file, {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01"
        })

    except ValidationException as e:
        print(e.message)
        exception_is_thrown = True

    assert exception_is_thrown == False


@pytest.mark.validator
def test_validator_missing_required_with_file():

    exception_is_thrown = False
    try:    
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        schema_file = os.path.join(absolute_path, "schemas/test-schema.json")
        validation = validate(schema_file, {
            "age": 30,
            "hired_at": "1970-01-01"
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.message=='Data could not be validated'
        assert e.errors=={'fields': {'name': 'name is required'}}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_with_non_existing_file():

    exception_is_thrown = False
    try:
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        schema_file = os.path.join(absolute_path, "schemas/does-not-exist.json")
        validation = validate(schema_file, {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01"
        })
    except ValidationException as e:
        print(e.message)
        exception_is_thrown = True
        assert e.message[-34:]=='does-not-exist.json does not exist'
    
    assert exception_is_thrown


@pytest.mark.validator
def test_validator_with_is_valid_1():

    exception_is_thrown = False
    try:
        validation = is_valid(schema=test_schema, data={
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01",
        })

    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True

    assert exception_is_thrown == False


@pytest.mark.validator
def test_validator_with_is_valid_wrong_format():

    exception_is_thrown = False
    try:
        validation = is_valid(schema=test_schema, data={
            "name": "Daniel",
            "age": 30,
            "hired_at": "last year",
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.errors=={'fields': {'hired_at': "'last year' is not a 'date'"}}

    assert exception_is_thrown


@pytest.mark.validator
def test_validator_with_is_valid_with_file():

    exception_is_thrown = False
    try:
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        schema_file = os.path.join(absolute_path, "schemas/test-schema.json")
        validation = is_valid(schema_file, {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01"
        })
        assert validation == True
    except ValidationException as e:
        exception_is_thrown = True

    assert exception_is_thrown == False


@pytest.mark.validator
def test_validator_with_is_valid_missing_required_with_file():

    exception_is_thrown = False
    try:
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        schema_file = os.path.join(absolute_path, "schemas/test-schema.json")
        validation = is_valid(schema_file, {
            "age": 30,
            "hired_at": "1970-01-01"
        })
    except ValidationException as e:
        print(e.errors)
        exception_is_thrown = True
        assert e.errors == {'fields': {'name': 'name is required'}}

    assert exception_is_thrown == True


@pytest.mark.validator
def test_validator_with_is_valid_with_non_existing_file():
    
    exception_is_thrown = False
    try:
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        schema_file = os.path.join(absolute_path, "schemas/does-not-exist.json")
        is_valid(schema_file, {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01"
        })
    except ValidationException as e:
        print(e.message)
        exception_is_thrown = True
        assert e.message[-34:] == "does-not-exist.json does not exist"

    assert exception_is_thrown

@pytest.mark.validator
def test_validator_with_is_valid_with_invalid_schema():
    
    exception_is_thrown = False
    try:
        is_valid(invalid_schema, {
            "name": "Daniel",
            "age": 30,
            "hired_at": "1970-01-01"
        })
    except ValidationException as e:
        exception_is_thrown = True
        print(e.message)
        assert e.message == "'sting' is not valid under any of the given schemas"

    assert exception_is_thrown
